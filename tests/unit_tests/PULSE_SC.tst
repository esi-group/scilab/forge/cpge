// ============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2013 - Scilab Enterprises - Bruno JOFRET
//
//  This file is distributed under the same license as the Scilab package.
// ============================================================================
//
// <-- ENGLISH IMPOSED -->
//
// <-- Short Description -->
// PULSE_SC

loadXcosLibs();

// Check where PULSE_SC block is defined
assert_checkequal(whereis("PULSE_SC"), "Sourceslib");
[a, b] = libraryinfo(whereis("PULSE_SC"));
assert_checkequal(b, strsubst("SCI/modules/scicos_blocks/macros/Sources/", "/", filesep()));

funcprot(0);
needcompile = 0;
alreadyran = %f;
%scicos_context = struct();

[status, message] = xcosValidateBlockSet("PULSE_SC");
if status == %f
    disp(message);
end
assert_checktrue(status);

disp(PULSE_SC("define"));