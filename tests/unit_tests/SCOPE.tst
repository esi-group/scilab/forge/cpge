// ============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2013 - Scilab Enterprises - Bruno JOFRET
//
//  This file is distributed under the same license as the Scilab package.
// ============================================================================
//
// <-- ENGLISH IMPOSED -->
//
// <-- Short Description -->
// SCOPE

loadXcosLibs();

// Check where SCOPE block is defined
assert_checkequal(whereis("SCOPE"), "cpgelib");

funcprot(0);
needcompile = 0;
alreadyran = %f;
%scicos_context = struct();

[status, message] = xcosValidateBlockSet("SCOPE");
if status == %f
    disp(message);
end
assert_checktrue(status);

disp(SCOPE("define"));
