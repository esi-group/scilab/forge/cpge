//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011-2011 - DIGITEO - Bruno JOFRET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function [x,y,typ]=RELAY(job,arg1,arg2)
    x=[];y=[];typ=[];
    select job
    case 'plot' then
        // deprecated
    case 'getinputs' then
        // deprecater
    case 'getoutputs' then
        // deprecated
    case 'getorigin' then
        // deprecated
    case 'set' then
        x=arg1;
        graphics=arg1.graphics;
        exprs=graphics.exprs
        model=arg1.model;

        while %t do
            [ok,Vmax,Vmin,tol,exprs]=scicos_getvalue('Relay parameters',..
            [gettext('Value when input is positiv');gettext('Value when input is negativ');gettext('Tolerance near 0')], ..
            list('vec',1,'vec',1,'vec',1),exprs)

            mess=[];
            if ~ok then
                // Cancel
                break;
            end

            if ok then
                model.rpar.objs(3).graphics.exprs(2)=list(['if u1>'+string(tol)+' then y1='+string(Vmax)+';'
                                    'elseif u1<-'+string(tol)+' then y1='+string(Vmin)+';'
                                    'else y1=0;'
                                    'end'
                                    ],"xd=[]","","","","",['';'y1=[]']);
                model.rpar.objs(3).model.opar=model.rpar.objs(3).graphics.exprs(2);                   
                graphics.exprs = exprs;
                x.model=model;
                x.graphics = graphics;
                break
            else
                message(mess);
            end



        end

    case 'define' then
        Vmax = 12;
        Vmin=-12;
        tol=1e-6;

       input_port=IN_f('define')
       input_port.graphics.exprs=["1"]
       input_port.model.ipar=[1]       
       input_port.graphics.pout=4
       
       output_port=OUT_f('define')
       output_port.graphics.exprs=["1"]
       output_port.model.ipar=[1]
       output_port.graphics.pin=5
       
       funct_user=scifunc_block_m('define');
       funct_user.graphics.exprs(1)=["[1,1]";"[1,1]";"[]";"[]";"[]";"[]";"[]";"[]";"0"]
       funct_user.graphics.exprs(2)=list(['if u1>1e-6 then y1=12;'
                                    'elseif u1<-1e-6 then y1=-12;'
                                    'else y1=0;'
                                    'end'
                                    ],"xd=[]","","","","",['';'y1=[]'])
       funct_user.graphics.pin=4
       funct_user.graphics.pout=5
       funct_user.model.opar=funct_user.graphics.exprs(2)
       
       diagram=scicos_diagram();
       diagram.objs(1)=input_port;
       diagram.objs(2)=output_port;
       diagram.objs(3)=funct_user;
       diagram.objs(4)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1, 1], from=[1, 1, 0], to=[3, 1, 1])   
       diagram.objs(5)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1, 1], from=[3, 1, 0], to=[2, 1, 1]) 
       
        model = scicos_model();
        model.sim='csuper'
        model.in=-1
        model.in2=-2
        model.intyp=-1
        model.out=-1
        model.out2=-2 
        model.outtyp=-1       
        model.blocktype='h'
        model.dep_ut=[%f %f]
        model.rpar=diagram
        
        x = standard_define([2 2], model, "", [])
        x.gui='RELAY'
        x.graphics.exprs=[string(Vmax);string(Vmin);string(tol)]
    end
endfunction

