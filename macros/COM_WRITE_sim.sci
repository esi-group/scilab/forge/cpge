//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011-2011 - DIGITEO - Bruno JOFRET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function block=COM_WRITE_sim(block,flag)
function DEBUG(message)
    disp("[DEBUG time = "+string(scicos_time())+"] {"+block.label+"} COM_WRITE Simulation: "+message);
endfunction
    select flag
     case -5 // Error

     case 0 // Derivative State Update


     case 1 // Output Update
      DEBUG("Output update ");
      // FIXME: Mettre ici tout ce qui sert a ecrire sur le port serie

      //u1 = block.inptr(1);
      //PIC_send(u1);



     case 2 // State Update

     case 3 // OutputEventTiming

     case 4 // Initialization

     case 5 // Ending
      // FIXME: quoi faire a la fin de la simulation

      //PIC_end_of_simul();

     case 6 // Re-Initialisation

     case 9 // ZeroCrossing

    else // Unknown flag

    end
endfunction