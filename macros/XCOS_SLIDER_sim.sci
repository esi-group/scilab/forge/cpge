//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2013 David Violeau
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function block=XCOS_SLIDER_sim(block,flag)

    global xcos_sliders 
    
  function DEBUG(message)
      disp("[DEBUG time = "+string(scicos_time())+"] {"+block.label+"} COM_READ Simulation: "+message);
  endfunction
  select flag
     case -5 // Error

     case 0 // Derivative State Update


     case 1 // Output Update

    // Necessité de mettre ici la recherche du bloc a partir de son nom car l'update du rep ne fonctionne pas dans xcos_buttons_display
        for i=1:size(xcos_sliders.names,'*')
            if block.opar(1)==xcos_sliders.names(i) then
                block.rpar(5)=i
            end
        end
        rep=block.rpar(5)
        block.outptr(1) =   xcos_sliders.sliders(rep).Value

     case 2 // State Update

     case 3 // OutputEventTiming

     case 4 // Initialization

     case 5 // Ending

     case 6 // Re-Initialisation

     case 9 // ZeroCrossing

    else // Unknown flag

    end
endfunction


function xcos_sliders_callback(xcos_sliders,num)
    global xcos_sliders
    
    xcos_sliders.textvalue(num).string='valeur = '+string(xcos_sliders.sliders(num).value)

endfunction
