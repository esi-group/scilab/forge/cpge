//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011-2011 - DIGITEO - Bruno JOFRET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function [x,y,typ]=TRAPEZOID(job,arg1,arg2)
    x=[];y=[];typ=[];
    select job
    case 'plot' then
        // deprecated
    case 'getinputs' then
        // deprecater
    case 'getoutputs' then
        // deprecated
    case 'getorigin' then
        // deprecated
    case 'set' then
        x=arg1;
        graphics=arg1.graphics;
        exprs=graphics.exprs
        model=arg1.model;
        while %t do
            [ok,amplitude,initial_value,start_time,rising_time,width_time,falling_time,exprs] = scicos_getvalue(...
            [msprintf(gettext("Set %s block parameters"), "TRAPEZOID");" "; ..
            "Génération d''un signal trapézoïdal";" "], ..
            [gettext("Amplitude"); gettext("Initial Value"); "Début du signal";"Temps de montée"; "Largeur du trapèze"; "Temps de descente"], ..
            list("vec",1,"vec",1,"vec",1,"vec",1,"vec",1,"vec",1), exprs);
            if ~ok then break end
            if ok then

                slope1_rising=amplitude/rising_time;
                slope1_falling=-1*amplitude/falling_time;  
                model.rpar.objs(1).model.rpar=[slope1_rising;start_time;initial_value];
                model.rpar.objs(1).graphics.exprs=[sci2exp(slope1_rising);sci2exp(start_time);sci2exp(initial_value)];
                model.rpar.objs(2).model.rpar=[slope1_rising;start_time+rising_time;0];
                model.rpar.objs(2).graphics.exprs=[sci2exp(slope1_rising);sci2exp(start_time+rising_time);sci2exp(0)];
                model.rpar.objs(3).model.rpar=[slope1_falling;start_time+rising_time+width_time;0];
                model.rpar.objs(3).graphics.exprs=[sci2exp(slope1_falling);sci2exp(start_time+rising_time+width_time);sci2exp(0)];
                model.rpar.objs(4).model.rpar=[slope1_falling;start_time+rising_time+width_time+falling_time;0];
                model.rpar.objs(4).graphics.exprs=[sci2exp(slope1_falling);sci2exp(start_time+rising_time+width_time+falling_time);sci2exp(0)];
                graphics.exprs=exprs
                x.graphics=graphics;
                x.model=model
                break
            end
        end


    case 'define' then

        amplitude=1
        rising_time=0.1
        start_time=0
        initial_value=0
        falling_time=0.1
        width_time=0.5
        slope1_rising=amplitude/rising_time;
        slope1_falling=-1*amplitude/falling_time;  
              
        diagram=scicos_diagram();
        ramp1=RAMP('define')
        ramp1.graphics.pout=5;
        ramp1.graphics.exprs=[sci2exp(slope1_rising);sci2exp(start_time);sci2exp(initial_value)];
        ramp1.model.rpar=[slope1_rising;start_time;initial_value];
        
        ramp2=RAMP('define')
        ramp2.graphics.pout=8;
        ramp2.graphics.exprs=[sci2exp(slope1_rising);sci2exp(start_time+rising_time);sci2exp(0)];
        ramp2.model.rpar=[slope1_rising;start_time+rising_time;0];
        
        ramp3=RAMP('define')
        ramp3.graphics.pout=9;
        ramp3.graphics.exprs=[sci2exp(slope1_falling);sci2exp(start_time+rising_time+width_time);sci2exp(0)];
        ramp3.model.rpar=[slope1_falling;start_time+rising_time+width_time;0];
                
        ramp4=RAMP('define')
        ramp4.graphics.pout=10;
        ramp4.graphics.exprs=[sci2exp(slope1_falling);sci2exp(start_time+rising_time+width_time+falling_time);sci2exp(0)];
        ramp4.model.rpar=[slope1_falling;start_time+rising_time+width_time+falling_time;0];
            
      sum_bloc=BIGSOM_f('define')
      sum_bloc.graphics.exprs="[1;-1;1;-1]";
      sum_bloc.graphics.pin=[7;8;9;10]
      sum_bloc.graphics.pout=[11]
      sum_bloc.graphics.in_implicit=["E";"E";"E";"E"];
      sum_bloc.model.in=[-1;-1;-1;-1];
      sum_bloc.model.in2=[1;1;1;1];
      sum_bloc.model.intyp=[1;1;1;1];
      sum_bloc.model.rpar=[1;-1;1;-1];
       
      output_port=OUT_f('define')
       output_port.graphics.exprs=["1"]
       output_port.model.ipar=[1]
       output_port.graphics.pin=11;

      diagram.objs(1)=ramp1;
      diagram.objs(2)=ramp2;
      diagram.objs(3)=ramp3;
      diagram.objs(4)=ramp4;
      diagram.objs(5)=sum_bloc;
      diagram.objs(6)=output_port;
      diagram.objs(7)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1, 1], from=[1, 1, 0], to=[5, 1, 1])
      diagram.objs(8)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1, 1], from=[2, 1, 0], to=[5, 2, 1])
      diagram.objs(9)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1, 1], from=[3, 1, 0], to=[5, 3, 1])  
      diagram.objs(10)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1, 1], from=[4, 1, 0], to=[5, 4, 1]) 
      diagram.objs(11)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1, 1], from=[5, 1, 0], to=[6, 1, 1]) 

        model = scicos_model();
        model.sim='csuper'
        model.out=-1
        model.out2=-2
        model.outtyp=-1
        model.blocktype='h'
        model.dep_ut=[%f %t]
        model.rpar=diagram
        x = standard_define([2 2], model, "", [])
        x.gui='TRAPEZOID'
        x.graphics.exprs=[string(amplitude);string(initial_value);string(start_time);string(rising_time);string(width_time);string(falling_time)]
    end
endfunction

