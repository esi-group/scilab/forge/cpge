//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011-2011 - DIGITEO - Bruno JOFRET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function [x,y,typ]=DIRAC(job,arg1,arg2)
	x = []; y = []; typ = [];
	select job
	case 'plot' then
		// deprecated
	case 'getinputs' then
		// deprecater
	case 'getoutputs' then
		// deprecated
	case 'getorigin' then
		// deprecated
	case 'set' then
		x = arg1;
		graphics = arg1.graphics;
		exprs = graphics.exprs;
		model = arg1.model;

		while %t do
			[ok, start_time, duree, exprs] = scicos_getvalue(..
				[msprintf(gettext("Set %s block parameters"), "DIRAC"); "En cas d''absence du Dirac sur la réponse, augmenter d''une décade la durée"; " "; gettext("DIRAC Function")], ..
				[gettext("Instant de l`impulsion");gettext("Durée de l`impulsion")], ..
				list("vec",1,"vec",1), ..
				exprs);

			if ~ok then
				break;
			end

			amplitude = 1/duree;

			if ok then
				model.rpar.objs(1).graphics.exprs = [sci2exp(start_time); "0"; sci2exp(amplitude)];
				// model.rpar.objs(1).model.rpar = [0, amplitude];
				// model.rpar.objs(1).model.firing = start_time;

				model.rpar.objs(2).graphics.exprs = [sci2exp(start_time+duree); "0"; sci2exp(amplitude)];
				// model.rpar.objs(2).model.rpar = [0, amplitude];
				// model.rpar.objs(2).model.firing = start_time+duree;

				graphics.exprs = exprs;
				x.graphics = graphics;
				x.model = model;
				break;
			end
		end

	case 'define' then
		start_time = 0;
		duree = 1e-7;
		amplitude = 1/duree;

		diagram = scicos_diagram();
		echelon1 = STEP('define');
		echelon1.graphics.pout = 5;
		echelon1.graphics.pein = 8;
		echelon1.graphics.peout = 8;
		echelon1.graphics.exprs = [sci2exp(start_time); "0"; sci2exp(amplitude)];
		// echelon1.model.rpar = [0, amplitude];
		// echelon1.model.firing = start_time;

		echelon2 = STEP('define');
		echelon2.graphics.pout = 6;
		echelon2.graphics.pein = 9;
		echelon2.graphics.peout = 9;
		echelon2.graphics.exprs = [sci2exp(start_time+duree); "0"; sci2exp(amplitude)];
		// echelon2.model.rpar = [0, amplitude];
		// echelon2.model.firing = start_time + duree;

		sum_bloc = SUMMATION('define');
		sum_bloc.graphics.pin = [5,6];
		sum_bloc.graphics.pout = [7];

		output_port = OUT_f('define');
		output_port.graphics.exprs = ["1"];
		output_port.model.ipar = [1];
		output_port.graphics.pin = 7;

		diagram.objs(1) = echelon1;
		diagram.objs(2) = echelon2;
		diagram.objs(3) = sum_bloc;
		diagram.objs(4) = output_port;
		diagram.objs(5) = scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1, 1], from=[1, 1, 0], to=[3, 1, 1]);
		diagram.objs(6) = scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1, 1], from=[2, 1, 0], to=[3, 2, 1]);
		diagram.objs(7) = scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1, 1], from=[3, 1, 0], to=[4, 1, 1]);
		diagram.objs(8) = scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[5, -1], from=[1, 1, 0], to=[1, 1, 1]);
		diagram.objs(9) = scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[5, -1], from=[2, 1, 0], to=[2, 1, 1]);

		model = scicos_model();
		model.sim = 'csuper';
		model.out = -1;
		model.out2 = -2;
		model.outtyp = -1;
		model.blocktype = 'h';
		model.dep_ut = [%f %t];
		model.rpar = diagram;

		x = standard_define([2 2], model, "", []);
		x.gui = 'DIRAC';
		x.graphics.exprs = [string(start_time); string(duree)];
	end
endfunction