

//Adaptation des datatips Hz en rad/s pour le tracé Nyquist
function nyquist_Hz2rad(h)

            ax=h.children(1)
            for c=1:size(ax.children,1)
                if ax.children(c).type=='Compound' then
                    for cc=1:size(ax.children(c).children,1)
                        if ax.children(c).children(cc).type=='Polyline' then
                            ax.children(c).children(cc).display_function = "NyquistTip";
                        elseif ax.children(c).children(cc).type=='Compound' then
                            for ccc=1:size(ax.children(c).children(cc).children,1)
                                for cccc=1:size(ax.children(c).children(cc).children(ccc).children,1)
                                    if ax.children(c).children(cc).children(ccc).children(cccc).type=='Text' then
                                        format('v',6) 
                                        if isnum(ax.children(c).children(cc).children(ccc).children(cccc).text) then
                                        ax.children(c).children(cc).children(ccc).children(cccc).text=string(evstr(ax.children(c).children(cc).children(ccc).children(cccc).text)*2*%pi)
                                        end
                                    end
                                end
                            end    
                        end
                    end
                elseif ax.children(c).type=='Legend' then
                    ax.children(c).background=8
                    ax.children(c).legend_location='upper_caption'
                end
                
            end    
            format('v',12) 
endfunction
