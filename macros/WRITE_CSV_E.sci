//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2013 David Violeau
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function [x,y,typ]=WRITE_CSV_E(job,arg1,arg2)
    x=[];y=[];typ=[];
    select job
    case 'plot' then
        // deprecated
    case 'getinputs' then
        // deprecater
    case 'getoutputs' then
        // deprecated
    case 'getorigin' then
        // deprecated
    case 'set' then
      x=arg1;
      graphics=arg1.graphics;
      exprs=graphics.exprs
      model=arg1.model;
      while %t do
       [ok, path, delimiter, nb_columns, exprs] = scicos_getvalue('Paramètres du bloc WRITE_CSV', ..
        [gettext("Chemin du fichier csv, eg:  E:\my path\file.txt"); gettext("Délimiteur de colonne (, ou ; ou s (pour un espace))"); gettext("Nombre de colonnes (autres que le temps)")], ..
        list("str",-1,"str",-1,"vec",1), exprs);
        if ~ok then break,end
        mess=[];
        if delimiter ~= "s" & delimiter ~= "," & delimiter ~= ";" then
            mess=[mess ;"Le délimiteur doit être une virgule (,) ou un point virgule (;) ou un s (pour un espace)"]
            ok = %f
        end
        if nb_columns<1 then
            mess=[mess ;"Le nombre de colonnes doit être plus grand que 1"]
            ok = %f
        end 
          
            if ok then
                out = ones(nb_columns,1);
                [model,graphics,ok]=set_io(model,graphics,list([out out],out),list(),ones(0,1),[]);
                
                diagram=scicos_diagram();
                clk=SampleCLK('define')
                clk.graphics.peout=3
                clk.graphics.exprs = ["0.1" ; "0"]
                clk.model.rpar= [0.1 ; 0]
                
                diagram.objs(1)=clk
                writecsv=WRITE_CSV("define")
                writecsv.graphics.exprs=exprs
                writecsv.graphics.pein=3
                [writecsv.model,writecsv.graphics,ok]=set_io(writecsv.model,writecsv.graphics,list([out out],out),list(),ones(1,1),[]);
                writecsv.model.rpar=[nb_columns, ascii(delimiter)];
                writecsv.model.ipar=[ascii(path)]
                writecsv.graphics.pin=[5:2:2*nb_columns+3]
                diagram.objs(2)=writecsv
                diagram.objs(3)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[5, -1], from=[1, 1, 0], to=[2, 1, 1])
                
                for i=1:nb_columns
                    diagram.objs(2*i-1+3)=IN_f('define')
                    diagram.objs(2*i-1+3).graphics.exprs=string(i)
                    diagram.objs(2*i-1+3).graphics.pout=2*i+3
                    diagram.objs(2*i-1+3).model.ipar=i
                    diagram.objs(2*i+3)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,1], from=[2*i-1+3, 1, 0], to=[2, i, 1])
                end

                model.rpar=diagram;
                graphics.exprs = exprs;
                x.model=model;
                x.graphics = graphics;
                break
            else
                message(mess);
            end



        end

    case 'define' then
        exprs=string(["./test.txt";",";"1"])

        diagram=scicos_diagram();
        diagram.objs(1)=SampleCLK('define')
        diagram.objs(1).graphics.peout=4
        diagram.objs(1).graphics.exprs=["0.1" ; "0"]
        diagram.objs(1).model.rpar = [0.1 ; 0]
        
        diagram.objs(2)=WRITE_CSV("define")
        diagram.objs(2).graphics.pein=4
        diagram.objs(2).graphics.pin=5
        diagram.objs(3)=IN_f('define')
        diagram.objs(3).graphics.pout=5
        diagram.objs(4)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[5, -1], from=[1, 1, 0], to=[2, 1, 1])
        diagram.objs(5)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1, 1], from=[3, 1, 0], to=[2, 1, 1])

        model = scicos_model();
        model.sim='csuper'
        model.in=1
        model.in2=1
        model.intyp=1
        model.blocktype='h'
        model.dep_ut=[%f %f]
        model.rpar=diagram
        x = standard_define([4 2], model, "", [])
        x.gui='WRITE_CSV_E'
        x.graphics.style=["blockWithLabel;verticalLabelPosition=center;displayedLabel=Write CSV"]
        x.graphics.exprs=exprs
    end
endfunction

