//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2013 David Violeau
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function [x, y, typ]=READ_CSV(job, arg1, arg2)
    x=[];
    y=[];
    typ=[];
    select job
     case 'plot' then
// deprecated
     case 'getinputs' then
// deprecater
     case 'getoutputs' then
// deprecated
     case 'getorigin' then
// deprecated
     case 'set' then
      x=arg1;
      graphics=arg1.graphics;exprs=graphics.exprs
      model=arg1.model;
      while %t do
       [ok, path, delimiter, nb_columns,nb_ligns_ignored, exprs] = scicos_getvalue([msprintf(gettext("Set %s block parameters"), "READ_CSV");" "; ..
        gettext("Get data from csv file");" "], ..
        [gettext("Path of csv file, eg:  E:\my path\file.txt"); gettext("delimiter (, or ; or t (tabulation))"); gettext("Number of column (the first one must be discrete time)"); gettext("Number of initial ligns ignored")], ..
        list("str",-1,"str",-1,"vec",1,"vec",1), exprs);
        if ~ok then break,end
        mess=[];
        if delimiter ~= "t" & delimiter ~= "," & delimiter ~= ";" then
            mess=[mess ;_("Delimiter must be , or ; or \t")]
            ok = %f
        end
        if nb_columns<2 then
            mess=[mess ;_("Number of column must be greater than 1")]
            ok = %f
        end
         if nb_ligns_ignored<0 then
            mess=[mess ;_("Number of column must be greater than 0")]
            ok = %f
        end       

        if ok then
          out = ones(nb_columns-1,1);
          [model,graphics,ok]=set_io(model,graphics,list(),list([out out],out),ones(1,1),[]);
          model.sim=list("READ_CSV_sim", 5)
          model.blocktype='d';
          model.dep_ut=[%t %f];
          model.firing=[0;-1]
          model.rpar=[nb_columns-1, ascii(delimiter),nb_ligns_ignored];
          model.ipar=[ascii(path)]
          graphics.exprs=exprs;
          x.graphics=graphics;x.model=model
          break
        end      
      end 
     case 'define' then
      model=scicos_model();
      k=1;
      model.sim=list("READ_CSV_sim", 5)
      model.blocktype='d';
      model.dep_ut=[%t %f];
      model.out=[1];
      model.evtin=[1];
      model.firing=[0;-1]
      exprs=string(["./test.txt",",","2","0"])
      model.rpar=[2,ascii(","),0];
      model.ipar=[ascii("./test.txt")];
      x=standard_define([4 2],model,exprs,[]);
      x.graphics.in_implicit=[];
      x.graphics.style=["blockWithLabel;verticalLabelPosition=center;displayedLabel= Read csv ;fillcolor=#FF3333"]
      x.graphics.out_implicit=['E'];
      
    end
endfunction
