// This file is released under the 3-clause BSD license. See COPYING-BSD.

function buildmacros()
  macros_path = get_absolute_file_path("buildmacros.sce");
  tbx_build_macros(TOOLBOX_NAME, macros_path);
  tbx_build_blocks(toolbox_dir, ["MAXPID", "REP_FREQ", "GRANDEUR_PHYSIQUE","PARAM_VAR", "COM_READ", "COM_WRITE", "COM_PROCESS",..
  "IMPRIMANTE","REP_TEMP","SCOPE","DIRAC","NI600X_C_READ","RELAY","TRAPEZOID",..
  "PIcontrol","PIDfiltered","READ_CSV","WRITE_CSV","PLAGE_LINEAIRE_INT","PLAGE_LINEAIRE_EXT","SEUIL","CRENEAU",'XCOS_BUTTON','XCOS_SLIDER',"REALTIME",...
  "READ_CSV_E","WRITE_CSV_E"]);
endfunction

buildmacros();
clear buildmacros; // remove buildmacros on stack

