//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011-2011 - DIGITEO - Bruno JOFRET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//
function y1=PIC_receive()
    global connected port_TCL isreceiv y1_prec inc t_prec
    //disp("receiv")
    if (isreceiv == 1) then
        if (connected==1) then
           [q,flags]=serialstatus(port_TCL)
           while(q<8)
             [q,flags]=serialstatus(port_TCL)
           end
           values=readserial(port_TCL);
           toto=ascii(values)';
           //disp(toto)
           y1(1)=toto(1)*256+toto(2);//position
           y1(2)=toto(3)*256+toto(4);//delta_position
           y1(3)=toto(7)*256+toto(8);//tension
           y1(4)=toto(5)*256+toto(6);//deltaT == 1 top = 0.1ms
           y1=double(int16(y1));
           y1(1)=y1(1)*0.042                        //passage des top en mm
           y1(2)=y1(2)/(toto(5)*256+toto(6))*10000*0.042; //multiplication par 10000 pour passer en top/s
                                                          //multiplication par 0.042 pour passer en mm/s
           y1(3)=y1(3)*12/2044;
           if(t_prec == -1) then
               y1(4)=0;
           else
               y1(4)=t_prec+(toto(5)*256+toto(6))/10000;  //on prend le temps global et on ajoute le nouveau temps
           end
           t_prec=y1(4);
           //disp(toto(5)*256+toto(6))
           //disp ('receiv')
           inc = inc + 1;
           //disp (inc)
        else
            //disp('Non connecté : receive '+string(y1_test))
            //y1 = y1_test;
            //inc = inc + 1
            //disp (inc)

        end
        isreceiv = 0
    end
endfunction
