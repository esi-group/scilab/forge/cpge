//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011-2011 - DIGITEO - Bruno JOFRET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//
function y1 = PIC_send(u1)
    global connected port_TCL isreceiv y1_test
//    disp(u1)
//    disp(connected)
    if (isreceiv == 0) then
        if (connected==1) then
                u1=u1*2044/12; //repassé en inc et non en volts
                u1=uint16(u1);
                values= "P" + ascii((int((u1)/256))) + ascii(modulo((u1),256))
                //disp('on envoie : '+values)
                writeserial(port_TCL,values);
        else
            disp('Non connecté : send '+string(u1))
            //disp(u1)
        end
        y1=u1
        y1_test = u1
        isreceiv = 1
    end
endfunction