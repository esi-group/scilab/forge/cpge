//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011-2011 - DIGITEO - Bruno JOFRET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function block=GRANDEUR_PHYSIQUE_sim(block,flag)
 
  if flag == 4 | flag == 6
// Initialisation || Re-Init
  end
  if flag ==  1
    // Output update
	block.outptr(1) = block.inptr(1);
  end

endfunction