//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012-2013 - David Fournier
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function [x,y,typ]=CRENEAU(job,arg1,arg2)
	x=[];y=[];typ=[];
	select job
	case 'plot' then
		// deprecated
	case 'getinputs' then
		// deprecater
	case 'getoutputs' then
		// deprecated
	case 'getorigin' then
		// deprecated
	case 'set' then
		x = arg1;
		graphics = arg1.graphics;
		exprs = graphics.exprs;
		model = arg1.model;

		while %t do
			[ok, start_time, duree, initial_value, final_value, exprs] = scicos_getvalue(..
				[msprintf(gettext("Set %s block parameters"), "CRENEAU");" "; gettext("CRENEAU Function");" "], ..
				[gettext("Instant initial"); gettext("Durée"); gettext("Valeur initiale et finale"); gettext("Valeur du créneau")], ..
				list("vec",1,"vec",1,"vec",1,"vec",1), ..
				exprs);

			if ~ok then
				break;
			end

			if ok then
				model.rpar.objs(1).graphics.exprs = [sci2exp(start_time); sci2exp(initial_value); sci2exp(final_value)];
				// model.rpar.objs(1).model.rpar = [initial_value; final_value];
				// model.rpar.objs(1).model.firing = start_time;

				model.rpar.objs(2).graphics.exprs = [sci2exp(start_time+duree); sci2exp(initial_value); sci2exp(final_value)];
				// model.rpar.objs(2).model.rpar = [initial_value; final_value];
				// model.rpar.objs(2).model.firing = start_time+duree;

				graphics.exprs = exprs;
				x.graphics = graphics;
				x.model = model;
				break;
			end
		end

	case 'define' then
		start_time = 1;
		duree = 2;
		initial_value = 0;
		final_value = 1;

		echelon1 = STEP('define');
		echelon1.graphics.pout = 5;
		echelon1.graphics.pein = 8;
		echelon1.graphics.peout = 8;
		echelon1.graphics.exprs = [sci2exp(start_time); sci2exp(initial_value); sci2exp(final_value)];
		// echelon1.model.rpar = [initial_value; final_value];
		// echelon1.model.firing = start_time;

		echelon2 = STEP('define');
		echelon2.graphics.pout = 6;
		echelon2.graphics.pein = 9;
		echelon2.graphics.peout = 9;  
		echelon2.graphics.exprs = [sci2exp(start_time+duree); sci2exp(initial_value); sci2exp(final_value)];
		// echelon2.model.rpar = [initial_value; final_value];
		// echelon2.model.firing = start_time+duree;

		sum_bloc = SUMMATION('define');
		sum_bloc.graphics.pin = [5;6];
		sum_bloc.graphics.pout = [7];
		// ou encore
		// sum_bloc = BIGSOM_f('define');
		// sum_bloc.graphics.exprs = "[1;-1]";
		// sum_bloc.graphics.pin = [5;6];
		// sum_bloc.graphics.pout = [7];
		// sum_bloc.graphics.in_implicit = ["E";"E"];
		// sum_bloc.model.in = [-1;-1];
		// sum_bloc.model.in2 = [1;1];
		// sum_bloc.model.intyp = [1;1];
		// sum_bloc.model.rpar = [1;-1];

		output_port = OUT_f('define');
		output_port.graphics.exprs = ["1"];
		output_port.model.ipar = [1];
		output_port.graphics.pin = 7;

		diagram = scicos_diagram();
		diagram.objs(1) = echelon1;
		diagram.objs(2) = echelon2;
		diagram.objs(3) = sum_bloc;
		diagram.objs(4) = output_port;
		diagram.objs(5) = scicos_link(xx=[0 ; 0], yy=[0 ; 0], ct=[1, 1], from=[1, 1, 0], to=[3, 1, 1]);
		diagram.objs(6) = scicos_link(xx=[0 ; 0], yy=[0 ; 0], ct=[1, 1], from=[2, 1, 0], to=[3, 2, 1]);
		diagram.objs(7) = scicos_link(xx=[0 ; 0], yy=[0 ; 0], ct=[1, 1], from=[3, 1, 0], to=[4, 1, 1]); 
		diagram.objs(8) = scicos_link(xx=[0 ; 0], yy=[0 ; 0], ct=[5, -1], from=[1, 1, 0], to=[1, 1, 1]);
		diagram.objs(9) = scicos_link(xx=[0 ; 0], yy=[0 ; 0], ct=[5, -1], from=[2, 1, 0], to=[2, 1, 1]);

		model = scicos_model();
		model.sim = 'super';
		model.out = -1;
		model.out2 = -2;
		model.outtyp = -1;
		model.blocktype = 'h';
		model.dep_ut = [%f, %t];
		model.rpar = diagram;

		x = standard_define([2 2], model, "", []);
		x.gui = 'CRENEAU';
		x.graphics.exprs = [string(start_time); string(duree); string(initial_value); string(final_value)];
	end
endfunction
