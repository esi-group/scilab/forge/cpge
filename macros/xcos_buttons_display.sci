//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2013 - David Violeau
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function [scs_m,err]=xcos_buttons_display(scs_m)
    global xcos_buttons figure_xcos_buttons
    err=0
    //Recherche des boutons dans le schema bloc et extraction des noms pour créer la structure xcos_buttons
     rep=1
     xcos_buttons.names=[]
     for i=1:size(scs_m.objs)
        obj=scs_m.objs(i);
        if typeof( obj )=='Block' & obj.gui=='XCOS_BUTTON' then
           xcos_buttons.names(rep)=obj.graphics.exprs(1)
           scs_m.objs(i).model.rpar(3)=rep 
           scs_m.objs(i).model.opar=list(obj.graphics.exprs(1))
           rep=rep+1
        end
    end

    if size(xcos_buttons.names,'*')>8 then
            message("Le nombre de boutons est limité à 8")
            err=1
            return ;
    elseif size(unique(xcos_buttons.names),'*')<>size(xcos_buttons.names,'*') then
            message("Vous devez donner des noms différents aux boutons")
            err=1
            return ;
    end
    
    // Effacement de la fenêtre des boutons si nécessaire
    //exist_fig=0
    list_fig=winsid()
    for i=list_fig
        h=scf(i)
        if h.Figure_name=='Xcos Buttons' then
            //exist_fig=1
            delete(h)
        end
    end

    //création de la fenêtre des boutons à la bonne taille (fonction du nombre de boutons)
    nb_xcos_buttons=size(xcos_buttons.names,'*')
    figSizeMax=nb_xcos_buttons*(18+84)
    //if ~exist_fig then
        figure_xcos_buttons = figure('Figure_name','Xcos Buttons','BackgroundColor',[1 1 1],'Position',[10,10,100,figSizeMax]);
        setlanguage('fr_FR')
        delmenu(figure_xcos_buttons.figure_id, gettext("File"));
        delmenu(figure_xcos_buttons.figure_id, gettext("Tools"));
        delmenu(figure_xcos_buttons.figure_id, gettext("Edit"));
        delmenu(figure_xcos_buttons.figure_id, gettext("?"));
        toolbar(figure_xcos_buttons.figure_id, "off");
    //end
    //clf(figure_xcos_buttons)

    // fonction permettant de remplir la figure f contenant les boutons xcos en spécifiant leur position dans la fenêtre (numero de placement de 1 à 8) et leur nom
    figSize = figure_xcos_buttons.axes_size;
    imgSize=[85 84] //taille des images des boutons
    xcos_buttons.button=list()
    xcos_buttons.img=list()
    xcos_buttons.textlabel=list()
    
    //xcos_buttons.path=SCI+'/contrib/CPGE/macros/images/'   
    [n,path_image_buttons]=libraryinfo('cpgelib'); 
    xcos_buttons.path=pathconvert( path_image_buttons  + filesep() +"images");
    xcos_buttons.figSize=figSize
    xcos_buttons.imgSize=imgSize
    path=xcos_buttons.path
    for i=1:nb_xcos_buttons
        position=[10 figSize(2)-i*(18+imgSize(2)) imgSize(1) imgSize(2)]
        text_position=[10 figSize(2)-i*(18+imgSize(2))-14 imgSize(1) 14]
        xcos_buttons.button(i) = uicontrol( "Parent", figure_xcos_buttons,"Style", "pushbutton", "Position", position,'BackgroundColor',[1 1 1],"units", "pixels","Value",0, "callback" , "xcos_button_callback(xcos_buttons,"+string(i)+")");
        xcos_buttons.img(i) = uicontrol("Parent", figure_xcos_buttons,"units", "pixels", "Style", "image",  "Position", position,..
        "String",path+"xcos_button_off.jpg", "Value", [1 1]);
        xcos_buttons.textlabel(i)=uicontrol("Parent", figure_xcos_buttons,"units", "pixels",'BackgroundColor',[1 1 1], "Style", "text","Position", text_position,"String","toto");
        xcos_buttons.textlabel(i).String= xcos_buttons.names(i)
    end

endfunction


