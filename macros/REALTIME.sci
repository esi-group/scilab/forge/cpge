//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2013 David Violeau
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function [x,y,typ]=REALTIME(job,arg1,arg2)
    x=[];y=[];typ=[];
    select job
    case 'plot' then
        // deprecated
    case 'getinputs' then
        // deprecater
    case 'getoutputs' then
        // deprecated
    case 'getorigin' then
        // deprecated
    case 'set' then
        x=arg1;
        graphics=arg1.graphics;
        exprs=graphics.exprs
        model=arg1.model;
        
        while (%t)  do
            [ok,var_temps,exprs]=scicos_getvalue('Simulation temps réel',..
                                                        [gettext('Durée en seconde d''une seconde réelle')], ..
                                                        list('vec',1), exprs)
          if ~ok then  break;  end

          if ok then
              model.rpar=var_temps;
              graphics.exprs =  exprs;
              x.model=model;
              x.graphics = graphics;
              break
          end
        end
     case 'define' then
        var_temps = 1;
        model=scicos_model();
        model.sim=list("REALTIME_sim",99) // 99 type blocks are ignored by simulator.
        model.blocktype='c';
        model.dep_ut=[%f %f];
        model.in=[];
        model.out=[];
        model.rpar=[var_temps];
        x=standard_define([4 2],model,[],[]);
        x.graphics.in_implicit=[];
        x.graphics.out_implicit=[];
        x.graphics.style=["blockWithLabel;verticalLabelPosition=center;displayedLabel= Realtime <br> 1s = %s s;fillcolor=#FF3333"]
        x.graphics.exprs= [string(var_temps)];
      
      
    end

endfunction
